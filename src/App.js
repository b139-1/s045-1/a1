import {Fragment} from "react";
import './App.css';
import {Container} from "react-bootstrap";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";

function App() {
  return (
    <Fragment>
      <AppNavbar/>
      <Container>
        <Home/>
      </Container>
    </Fragment>
  )
}

export default App;
