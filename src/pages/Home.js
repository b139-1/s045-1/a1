import {Fragment} from "react";
import {Container} from "react-bootstrap";
import Banner from "./../components/Banner";
import Highlights from "./../components/Highlights";
import CourseCard from "./../components/CourseCard"

const Home = () => {
	return (
		<Fragment>
		  <Banner/>
		  <Highlights/>
		  <CourseCard/>
		</Fragment>
	)
}

export default Home;