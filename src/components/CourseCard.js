import {Container, Row, Col, Card, Button} from "react-bootstrap"


const CourseCard = () => {
	return (
		<Container className="mb-4">
			<Row>
				<Col>
					<Card>
					  <Card.Body>
					  		<Card.Title>Sample Course</Card.Title>
					    	<Card.Text>
					    	Description:
				    		<p>Lorem ipsum dolor sit amet consectetur adipisicing, elit. Maiores adipisci, ducimus totam eius. Error maiores hic, nihil autem eos deleniti reprehenderit, eum voluptas minima pariatur consequatur, quis aspernatur.</p>
				    		Price:
				    		<p>Php 40,000</p>
					    	</Card.Text>

					    	<Button variant="primary">Enroll</Button>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}

export default CourseCard;