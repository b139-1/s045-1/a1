import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


/*const name = "John Smith"
const element = <h1>Hello, {name}</h1>

ReactDOM.render(
    element, document.getElementById('root')
);*/

// Mini activity
// create another variable that is object which contains properties firstname and lastname
// using a function, join the firstname and lastname and display it in the browser using ReactDOM.render()
/*const person = {
  firstname: "Kaiser",
  lastname: "Tabuada"
}

const profile = () => {
  const display = <h1>Hello, {person.firstname} {person.lastname}</h1>
  return display
}

ReactDOM.render(
    profile(), document.getElementById('root')
);*/